Code Challenge
I've used mailgun as the mail client

Backend is Node
Frontend is React

All the build commands will work with either yarn or npm.

To setup the solution:

Clone the repo

Navigate to the project folder via terminal, run: install

Start the api with: start

Navigate to /webapp via terminal, run: install

Start the frontend with: start

The api has limited tests attached to it, these an be run via: test

Mailgun only allows emailing to Authorised emails on the trial account. For this reason I have sent an Authorisation email to mark@hypothesisconsulting.com.au, further accounts can be added if there is a requirement for it.





