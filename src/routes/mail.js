import express from 'express';

const router = new express.Router();

// Require controller module
const mailController = require('../controllers/mailController');

/**
 * @swagger
 * /mail:
 *   post:
 *     summary: email send
 *     description: sends an email using the provided details
 *     tags:
 *       - mail
 *     parameters:
 *       - name: mailRequest
 *         in: body
 *         required: true
 *         description: mail request object
 *         schema:
 *           type: object
 *           properties:
 *             sender:
 *               type: string
 *               default: null
 *             recipients:
 *               type: object
 *               properties:
 *                  name: 
 *                    type: string
 *                    default: null
 *                  email:
 *                    type: string
 *                    default: null
 *             carbonCopies:
 *               type: object
 *               properties:
 *                  name: 
 *                    type: string
 *                    default: null
 *                  email:
 *                    type: string
 *                    default: null
 *             blindCopies:
 *               type: object
 *               properties:
 *                  name: 
 *                    type: string
 *                    default: null
 *                  email:
 *                    type: string
 *                    default: null
 *             subject:
 *               type: string
 *               default: null
 *             message:
 *               type: string
 *               default: null
 *     responses:
 *       200:
 *         description: success 
 */
router.post('/', mailController.sendMail);

export default router;
