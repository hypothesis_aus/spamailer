import 'babel-polyfill';
import express from 'express';
import bodyParser from 'body-parser';
import routes from 'routes';
import cors from 'cors';
import log from 'log';
import { errorHandle } from 'utils';

// error handle
process.on('unhandledRejection', err => {
  throw err;
});

process.on('uncaughtException', err => {
  log.error('uncaughtException:', err);
});

const app = express();

app.use(cors());
app.use(bodyParser.json({
  limit: '50mb'
}));

app.use('/', routes);

app.use(errorHandle);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  log.info(`App is listening on ${port}.`);
});

export default app;
