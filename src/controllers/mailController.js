import emailValidator from '../utils/emailValidator';
import emailSenders from '../utils/emailSenders';

exports.sendMail = async (req, res, next) => {
    try {
        console.log(req.body);
        if(!req.body.sender || !req.body.subject || !req.body.message || !req.body.recipients){
            return next({ msg: 'Bad Request', status: 400 });
        }

        const sender = req.body.sender.replace(/\s/g, '');
        const recipients = req.body.recipients;
        const carbonCopies = req.body.carbonCopies;
        const blindCopies = req.body.blindCopies;
        const subject = req.body.subject;
        const message = req.body.message;

        
        if(!recipients || !emailValidator.validateRecipients(recipients)) {
            return next({ msg: 'One or more of the recipient emails is invalid, please check them', status: 400 });
        }
        if(carbonCopies && !emailValidator.validateRecipients(carbonCopies)) {
            return next({ msg: 'One or more of the cc emails is invalid, please check them', status: 400 });
        }
        if(blindCopies && !emailValidator.validateRecipients(blindCopies)) {
            return next({ msg: 'One or more of the bcc emails is invalid, please check them', status: 400 });
        }
        
        emailSenders.sendEmail(sender,recipients,carbonCopies,blindCopies,subject,message, (resp) => {
            return next(resp);
        });
    } catch (err) {
        next(err);
    }
};