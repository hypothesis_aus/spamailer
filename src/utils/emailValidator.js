const emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

exports.validateRecipients = function (emails) {
    for(var i = 0; i < emails.length; i++){
        if(!emailRegex.test(emails[i])){
            return false;
        }
    }
    return true;
};
  