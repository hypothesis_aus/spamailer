import request from 'request';
import querystring from 'querystring'

exports.sendEmail = function(sender, recipients, carbonCopies, blindCopies, sub, message, next){
    let data = { 
        from: sender + '@sandboxaca12acdb21f48efb2bb1e3bc3fe0a92.mailgun.org', 
        to: recipients.join(), 
        subject: sub, 
        text: message 
    }
    if(carbonCopies){
        data.cc = carbonCopies;
    }
    if(blindCopies){
        data.bcc = blindCopies;
    }

    var formattedData = querystring.stringify(data);
    request({
        headers: {
            'Content-Length': formattedData.length,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        uri: 'https://{INSERTAPIKEYHERE}@api.mailgun.net/v3/sandboxaca12acdb21f48efb2bb1e3bc3fe0a92.mailgun.org/messages',
        body: formattedData,
        method: 'POST'
    }, function (error, response, body) {
        var bodyObject = JSON.parse(body);
        if (!error && response.statusCode == 200) {
            return next({ msg: 'Email Sent Successfully ' + bodyObject.message, status: response.statusCode});
        }
        return next({ msg: 'Mailgun Encountered an issue with your request: ' + bodyObject.message, status: response.statusCode});
    });
}