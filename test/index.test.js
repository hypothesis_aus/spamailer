import 'babel-polyfill';
import app from '../src/main';
const request = require('supertest')(app);
const expect = require('chai').expect;

describe('test routes/index.js', async () => {
  it('should return LIVE', (done) => {
    request.get('/')
    .expect(200)
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.msg).to.equal('LIVE');
      done();
    });
  });
});

describe('test empty load', async () => {
  it('should return Bad Request when post empty', (done) => {
    request.post('/mail')
    .expect(400)
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.msg).to.equal('Bad Request');
      done();
    });
  });
});

describe('test base malformed handling', async () => {
  it('should return Bad Request when post is malformed', (done) => {
    request.post('/mail')
    .expect(400)
    .send({ sender: 'test@test.com', recipients: ["test@test.com"], message:'TEST MSG' })
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.msg).to.equal('Bad Request');
      done();
    });
  });
});

describe('test recipients load', async () => {
  it('should return the correct error when post recipients are malformed', (done) => {
    request.post('/mail')
    .expect(400)
    .send({ sender: 'test@test.com', recipients: ["asd"], subject: 'TEST', message:'TEST MSG' })
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.msg).to.equal('One or more of the recipient emails is invalid, please check them');
      done();
    });
  });
});

describe('test cc load', async () => {
  it('should return the correct error when post ccs are malformed', (done) => {
    request.post('/mail')
    .expect(400)
    .send({ sender: 'test@test.com', recipients: ["test@test.com"], subject: 'TEST', carbonCopies:["asd"], message:'TEST MSG' })
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.msg).to.equal('One or more of the cc emails is invalid, please check them');
      done();
    });
  });
});

describe('test bcc load', async () => {
  it('should return the correct error when post bccs are malformed', (done) => {
    request.post('/mail')
    .expect(400)
    .send({ sender: 'test@test.com', recipients: ["test@test.com"], subject: 'TEST', blindCopies: ["asd"], message:'TEST MSG' })
    .end((err, res) => {
      if (err) return done(err);
      expect(res.body.msg).to.equal('One or more of the bcc emails is invalid, please check them');
      done();
    });
  });
});