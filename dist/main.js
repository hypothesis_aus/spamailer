'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('babel-polyfill');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _log = require('./log');

var _log2 = _interopRequireDefault(_log);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// error handle
process.on('unhandledRejection', err => {
  throw err;
});

process.on('uncaughtException', err => {
  _log2.default.error('uncaughtException:', err);
});

const app = (0, _express2.default)();

app.use((0, _cors2.default)());
app.use(_bodyParser2.default.json({
  limit: '50mb'
}));

app.use('/', _routes2.default);

app.use(_utils.errorHandle);

const port = process.env.PORT || 5000;
app.listen(port, () => {
  _log2.default.info(`App is listening on ${port}.`);
});

exports.default = app;