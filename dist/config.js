"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const env = process.env.NODE_ENV;
const common = {
  port: 8880
};
const config = {
  develop: {},
  production: {},
  test: {}
};
exports.default = Object.assign(common, config[env]);