'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const router = new _express2.default.Router();

/**
 * @swagger
 * /mail:
 *   post:
 *     summary: email send
 *     description: sends an email using the provided details
 *     tags:
 *       - mail
 *     parameters:
 *       - name: mailRequest
 *         in: body
 *         required: true
 *         description: mail request object
 *         schema:
 *           type: object
 *           properties:
 *             sender:
 *               type: string
 *               default: null
 *             receiver:
 *               type: string
 *               default: null
 *             message:
 *               type: string
 *               default: null
 *     responses:
 *       200:
 *         description: success
 */
router.post('/', (() => {
  var _ref = _asyncToGenerator(function* (req, res) {
    res.send({ msg: 'HELLO WORLD' });
  });

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
})());

router.post('/login', (() => {
  var _ref2 = _asyncToGenerator(function* (req, res, next) {
    const { name, password } = req.body;
    try {
      if (true) {
        return res.send({ msg: 'HELLO WORLD' });
      }
      next({ msg: 'wrong username or password', status: 401 });
    } catch (err) {
      next(err);
    }
  });

  return function (_x3, _x4, _x5) {
    return _ref2.apply(this, arguments);
  };
})());

exports.default = router;