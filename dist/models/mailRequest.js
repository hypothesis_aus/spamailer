"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
const MailRequest = {
    Sender: String,
    Receiver: String,
    Message: String
};

exports.default = MailRequest;