'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mailRequest = undefined;

var _mailRequest = require('./mailRequest');

var _mailRequest2 = _interopRequireDefault(_mailRequest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.mailRequest = _mailRequest2.default;