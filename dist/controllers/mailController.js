'use strict';

var _emailValidator = require('../utils/emailValidator');

var _emailValidator2 = _interopRequireDefault(_emailValidator);

var _emailSenders = require('../utils/emailSenders');

var _emailSenders2 = _interopRequireDefault(_emailSenders);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.sendMail = (() => {
    var _ref = _asyncToGenerator(function* (req, res, next) {
        try {
            console.log(req.body);
            if (!req.body.sender || !req.body.subject || !req.body.message || !req.body.recipients) {
                return next({ msg: 'Bad Request', status: 400 });
            }

            const sender = req.body.sender.replace(/\s/g, '');
            const recipients = req.body.recipients;
            const carbonCopies = req.body.carbonCopies;
            const blindCopies = req.body.blindCopies;
            const subject = req.body.subject;
            const message = req.body.message;

            if (!recipients || !_emailValidator2.default.validateRecipients(recipients)) {
                return next({ msg: 'One or more of the recipient emails is invalid, please check them', status: 400 });
            }
            if (carbonCopies && !_emailValidator2.default.validateRecipients(carbonCopies)) {
                return next({ msg: 'One or more of the cc emails is invalid, please check them', status: 400 });
            }
            if (blindCopies && !_emailValidator2.default.validateRecipients(blindCopies)) {
                return next({ msg: 'One or more of the bcc emails is invalid, please check them', status: 400 });
            }

            _emailSenders2.default.sendEmail(sender, recipients, carbonCopies, blindCopies, subject, message, function (resp) {
                return next(resp);
            });
        } catch (err) {
            next(err);
        }
    });

    return function (_x, _x2, _x3) {
        return _ref.apply(this, arguments);
    };
})();