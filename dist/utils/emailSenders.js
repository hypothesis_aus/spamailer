'use strict';

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _querystring = require('querystring');

var _querystring2 = _interopRequireDefault(_querystring);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.sendEmail = function (sender, recipients, carbonCopies, blindCopies, sub, message, next) {
    let data = {
        from: sender + '@sandboxaca12acdb21f48efb2bb1e3bc3fe0a92.mailgun.org',
        to: recipients.join(),
        subject: sub,
        text: message
    };
    if (carbonCopies) {
        data.cc = carbonCopies;
    }
    if (blindCopies) {
        data.bcc = blindCopies;
    }

    var formattedData = _querystring2.default.stringify(data);
    (0, _request2.default)({
        headers: {
            'Content-Length': formattedData.length,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        uri: 'https://api:key-30d50efb84c34519629d77e750731b93@api.mailgun.net/v3/sandboxaca12acdb21f48efb2bb1e3bc3fe0a92.mailgun.org/messages',
        body: formattedData,
        method: 'POST'
    }, function (error, response, body) {
        var bodyObject = JSON.parse(body);
        if (!error && response.statusCode == 200) {
            return next({ msg: 'Email Sent Successfully ' + bodyObject.message, status: response.statusCode });
        }
        return next({ msg: 'Mailgun Encountered an issue with your request: ' + bodyObject.message, status: response.statusCode });
    });
};