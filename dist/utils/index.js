'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.errorHandle = exports.swagDocHandler = undefined;

var _swagDocHandler = require('./swagDocHandler');

var _swagDocHandler2 = _interopRequireDefault(_swagDocHandler);

var _errorHandle = require('./errorHandle');

var _errorHandle2 = _interopRequireDefault(_errorHandle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.swagDocHandler = _swagDocHandler2.default;
exports.errorHandle = _errorHandle2.default;