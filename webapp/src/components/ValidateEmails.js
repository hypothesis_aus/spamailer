// eslint-disable-next-line
const emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

export const ValidateEmails = (emailList) => {
    for(var i = 0; i < emailList.length; i++){
        if(!emailRegex.test(emailList[i])){
            return false;
        }
    }
    return true;
};

export default ValidateEmails;
