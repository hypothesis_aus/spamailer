import React, { Component } from 'react';
import './MailForm.css';
import ValidateEmails from './ValidateEmails';

class MailForm extends Component {
    constructor(props) {
        super(props);
        this.state = { sender: '', recipients: '', carbonCopies: '', blindCopies: '', subject:'', message:'' }
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange = (e) => {
        let newState = {};
        newState[e.target.name] = e.target.value;
        this.setState(newState);
    };
    
    handleSubmit = (e, message) => {
        e.preventDefault();
        var self = this;
        self.setState({ error: ''});
        
        let postData = {
            sender: self.state.sender,
            recipients: self.state.recipients.split(';'),
            carbonCopies: self.state.carbonCopies.length > 0 ? self.state.carbonCopies.split(';') : null,
            blindCopies: self.state.blindCopies.length > 0 ? self.state.blindCopies.split(';') : null,
            subject: self.state.subject,
            message: self.state.message,
        }

        if(!ValidateEmails(postData.recipients)){
            self.setState({ error: 'One of the recipient email addresses you entered was not valid'});
            return false;
        }

        if(postData.carbonCopies && !ValidateEmails(postData.carbonCopies)){
            self.setState({ error: 'One of the carbon copy email addresses you entered was not valid'});
            return false;
        }

        if(postData.blindCopies && !ValidateEmails(postData.blindCopies)){
            self.setState({ error: 'One of the blind copy email addresses you entered was not valid'});
            return false;
        }
    
        fetch('http://localhost:5000/mail', {
            method: "POST",
            body: JSON.stringify(postData),
            headers: {
                "Content-Type": "application/json"
            },
        }).then(function(response) {
            if(response.status === 404){
                self.setState({ error: 'The server seems to not be responding at the moment. Check that the API is running.'});
            }
            if(response.status === 400){
                self.setState({ error: 'The data was malformed, please refresh the browser and try again, if this happens again please contact support'});
            }
            if(response.status === 200){
                self.setState({ error: 'Emails Sent'});
            }
            
            self.setState({
                sender: '',
                recipients: '',
                carbonCopies: '',
                blindCopies: '',
                subject: '',
                message: '',
            });

            return response.text()
        }, function(error) {
            self.setState({ error: 'This service has enocountered a server error. Please inform support.'});
        })
    };
  
    render() {
        return (
            <form className='react-form' onSubmit={this.handleSubmit}>
                <h3>To proceed please fill in the required fields below</h3>
                <div>{this.state.error}</div>
                <fieldset className='form-group'>
                    <input id='sender' className='form-input' name='sender' placeholder='Your name' type='text' required onChange={this.handleChange} value={this.state.sender} />
                </fieldset>
                <fieldset className='form-group'>
                    <input id='recipients' className='form-input' name='recipients' placeholder='Recipients (semicolon separated)' type='text' required onChange={this.handleChange} value={this.state.recipients} />
                </fieldset>
                <fieldset className='form-group'>
                    <input id='carbonCopies' className='form-input' name='carbonCopies'  placeholder='Carbon Copies (optional, semicolon separated)' type='text'  onChange={this.handleChange} value={this.state.carbonCopies} />
                </fieldset>
                <fieldset className='form-group'>
                    <input id='blindCopies' className='form-input' name='blindCopies' placeholder='Blind Carbon Copies (optional, semicolon separated)' type='text'  onChange={this.handleChange} value={this.state.blindCopies} />
                </fieldset>
                <fieldset className='form-group'>
                    <input id='subject' className='form-input' name='subject' type='text' placeholder='Subject Line' required onChange={this.handleChange} value={this.state.subject} />
                </fieldset>
                <fieldset className='form-group'>
                    <textarea id='message' className='form-textarea' name='message' placeholder='Message text' required onChange={this.handleChange} value={this.state.message}></textarea>
                </fieldset>
                <div className='form-group'>
                    <input id='formButton' className='btn' type='submit' placeholder='Send message' />
                </div>
            </form>
        );
    }
}

export default MailForm;
