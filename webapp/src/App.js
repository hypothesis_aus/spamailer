import React, { Component } from 'react';
import MailForm from './components/MailForm';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  // Initialize state

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Code Challenge: Mailer</h2>
        </div>
        <div>
          <MailForm />
        </div>
      </div>
    );
  }
}

export default App;
